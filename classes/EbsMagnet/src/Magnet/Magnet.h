#ifndef EBSMAGNET_MAGNET_H
#define EBSMAGNET_MAGNET_H
#include <Quadrupole.h>
#include <tango/classes/EbsMagnet/EbsMagnet.h>

namespace EbsMagnet_ns {

class EbsMagnet;
class DynAttribute;

typedef struct ConfigItem {
  std::string name;
  std::string value;
} ConfigItem;

union value {
  Tango::DevBoolean bValue;
  Tango::DevDouble dValue;
};

typedef struct AttributeItem {

  DynAttribute *handle;  // Use only at init time
  std::string name;
  value read;
  value set;

} AttributeItem;

class Magnet {

public:

  // Create a Magnet
  static Magnet *Create(EbsMagnet *ds, bool focusing,double crossTalk, std::string &serial, std::string &type, std::string &model);

  // Constructor
  Magnet(EbsMagnet *ds, std::string& type, std::string& serial, std::string& modelName);
  ~Magnet();

  // Return attributes of this magnet
  // Use only at initialisation time, Tango may delete them
  std::vector<DynAttribute *> getAttributes();

  // Return setpointCheckCommand
  std::vector<std::string> getSetpointCheckCommands();

  // Add attribute to the magnet
  AttributeItem *addAttribute(const std::string& name,long data_type,Tango::AttrWriteType rwType,const std::string& unit,
                              const std::string& format,bool memorized,bool memorized_init);

  // Has a main current ?
  bool hasMainCurrent() {
    return model->has_main_current();
  }

  // Number of strengths
  size_t getStrengthNumber() {
    return nbStrengths;
  }

  // Number of currents
  size_t getCurrentNumber() {
    return nbCurrents;
  }

  // Read attribute
  void readAttribute(Tango::Attribute &attr,std::string& name,void *readValue=nullptr);
  void readStrengths(double *strengths);

  // Write attribute
  void writeAttribute(double setValue,const std::string& name,std::vector<std::string>& psNames,std::vector<Tango::DevState>& psStates);
  void writeAttribute(bool setValue,const std::string& name,std::vector<std::string>& psNames,std::vector<Tango::DevState>& psStates);
  void writeStrengths(std::vector<double>& strengths);

  // Main is frozen
  bool isMainFrozen();

  // Setpoint check
  bool setpointCheck(double strength,std::string& name);
  bool setpointCheck(std::vector<double>& strengths);

  // Set currents (update
  void setCurrentsAndUpdateStrength(std::vector<double>& currents, std::vector<double>& setCurrents);

  // Compute pseudo current (SH only)
  void computePseudoCurrentsFromCurrents(std::vector<double>& currents, std::vector<double>& pseudoCurrents);
  void computeStrengthFromPseudoCurrent(int idx,double& pseudoCurrent, double& strength);

  // Set design strength
  bool setDesignStrength(double strength);

  // Reset resonance strength
  void resetResonanceStrength();

  // Get correction strength setpoint
  double getCorrectionStrength();

  // Get resonance strength setpoint
  double getResonanceStrength();
  void setResonanceStrength(double set);

  // send currents to PS
  void send_currents();

  // Util split function
  static void split(std::vector<std::string> &tokens, const std::string &text, char sep);
  static bool compare(const std::string& s1,const std::string& s2);
  static std::string getStrengthPrefix(const std::string& name);

  
protected:

  EbsMagnet *ds;
  std::string modelName;
  std::string serial;
  size_t nbStrengths;
  size_t nbCurrents;
  std::string type;

  // shortcuts
  AttributeItem *currentAttr;
  AttributeItem *strengthAtt;
  AttributeItem *designStrengthsAtt;
  AttributeItem *correctionStrengthAtt;
  AttributeItem *resonanceStrengthAtt;
  AttributeItem *frozenAtt;

  // Strength/Current calculation
  MagnetModel::Magnet *model;   // Model
  std::vector<ConfigItem> config;    // Model data
  void parseConfig();
  std::string getConfigItem(const std::string& name);
  double getConfigItemDouble(const std::string& name);

  // Attributes
  void createAttributes();
  std::vector<AttributeItem> scalarAttributes;
  AttributeItem *getAttribute(const std::string& name);

  // Update correction strength
  void updateCorrectionStrength();

  // Check PS setpoints
  bool check_minmax(std::vector<double>& currents,bool throwException = false);

  // Freeze check
  void frozenCheck(const std::string& name);

  // Check underlying PS
  void psCheck(const std::string& name,std::vector<std::string>& names,std::vector<Tango::DevState>& states);

  // Utils functions
  static bool isStrengthH(const std::string& name);
  static bool isStrengthV(const std::string& name);
  static bool isStrengthSQ(const std::string& name);
  static bool isStrength(const std::string& name);
  static bool isStrengths(const std::string& name);
  static bool isCurrent(const std::string& name);
  static bool isMainCurrent(const std::string& name);
  static bool isFrozen(const std::string& name);
  static bool isResonanceStrength(const std::string& name);
  static bool isCorrectionStrength(const std::string& name);

};

} // end namespace EbsMagnet_ns

#endif //EBSMAGNET_MAGNET_H

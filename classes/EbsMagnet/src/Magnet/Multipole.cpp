#include "Multipole.h"

namespace EbsMagnet_ns {

// -----------------------------------------------------------------------------------------
Quadrupole::Quadrupole(EbsMagnet *ds, bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type):
            Magnet(ds,type,serial,magModel) {

  // Init model
  auto *quad = new MagnetModel::Quadrupole();
  try {
    quad->init(focusing,
               crossTalk*getConfigItemDouble("scale"),
               getConfigItem("_path")+getConfigItem("strength"),
               getConfigItem("_path")+getConfigItem("params"),
               serial);
  } catch(std::invalid_argument& e) {
    Tango::Except::throw_exception("QuadrupoleConfigError",
                                   e.what(),
                                   "Magnet::parseConfig");
  }
  model = quad;

  // Create attributes
  createAttributes();

}

// -----------------------------------------------------------------------------------------
Sextupole::Sextupole(EbsMagnet *ds, bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type):
        Magnet(ds,type,serial,magModel) {

  // Init model
  auto *sextu = new MagnetModel::Sextupole();
  try {
    sextu->init(getConfigItem("_path")+getConfigItem("strength"),
                getConfigItem("_path")+getConfigItem("params"),
                serial);
  } catch(std::invalid_argument& e) {
    Tango::Except::throw_exception("SextupoleConfigError",
                                   e.what(),
                                   "Magnet::parseConfig");
  }
  model = sextu;

  // Create attributes
  createAttributes();

}

// -----------------------------------------------------------------------------------------
Octupole::Octupole(EbsMagnet *ds, bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type):
        Magnet(ds,type,serial,magModel) {

  // Init model
  auto *octu = new MagnetModel::Octupole();
  try {
    octu->init(focusing,
               crossTalk*getConfigItemDouble("scale"),
               getConfigItem("_path")+getConfigItem("strength"),
               getConfigItem("_path")+getConfigItem("params"),
               serial);
  } catch(std::invalid_argument& e) {
    Tango::Except::throw_exception("OctupoleConfigError",
                                   e.what(),
                                   "Magnet::parseConfig");
  }
  model = octu;

  // Create attributes
  createAttributes();

}

// -----------------------------------------------------------------------------------------
DipoleQuadrupole::DipoleQuadrupole(EbsMagnet *ds, bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type):
        Magnet(ds,type,serial,magModel) {

  // Init model
  auto *dq = new MagnetModel::DipoleQuadrupole();
  try {
    dq->init(focusing,
             crossTalk*getConfigItemDouble("scale"),
             getConfigItem("_path")+getConfigItem("strength"),
             getConfigItem("_path")+getConfigItem("params"),
             getConfigItem("_path")+getConfigItem("strength_h"),
             getConfigItem("_path")+getConfigItem("matrix"),
             serial);
  } catch(std::invalid_argument& e) {
    Tango::Except::throw_exception("DipoleQuadrupoleConfigError",
                                   e.what(),
                                   "Magnet::parseConfig");
  }
  model = dq;

  // Create attributes
  createAttributes();

}

// -----------------------------------------------------------------------------------------
SH3Magnet::SH3Magnet(EbsMagnet *ds, bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type):
        Magnet(ds,type,serial,magModel) {

  // Init model
  auto *sh3 = new MagnetModel::SH3Magnet();
  try {
    sh3->init(crossTalk*getConfigItemDouble("scale"),
              getConfigItem("_path")+getConfigItem("strength_h"),
              getConfigItem("_path")+getConfigItem("strength_v"),
              getConfigItem("_path")+getConfigItem("strength_sq"),
              getConfigItem("_path")+getConfigItem("matrix"));
  } catch(std::invalid_argument& e) {
    Tango::Except::throw_exception("SH3MagnetConfigError",
                                   e.what(),
                                   "Magnet::parseConfig");
  }
  model = sh3;

  // Create attributes
  createAttributes();

}

// -----------------------------------------------------------------------------------------
SH5Magnet::SH5Magnet(EbsMagnet *ds, bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type):
        Magnet(ds,type,serial,magModel) {

  // Init model
  auto *sh5 = new MagnetModel::SH5Magnet();
  try {
    sh5->init(crossTalk*getConfigItemDouble("scale"),
              getConfigItem("_path")+getConfigItem("strength"),
              getConfigItem("_path")+getConfigItem("strength_h"),
              getConfigItem("_path")+getConfigItem("strength_v"),
              getConfigItem("_path")+getConfigItem("strength_sq"),
              getConfigItem("_path")+getConfigItem("matrix"));
  } catch(std::invalid_argument& e) {
    Tango::Except::throw_exception("SH5MagnetConfigError",
                                   e.what(),
                                   "Magnet::parseConfig");
  }
  model = sh5;

  // Create attributes
  createAttributes();

}

}

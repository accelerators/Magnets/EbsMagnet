#include "Multipole.h"
#include <tango/classes/EbsMagnet/EbsMagnetClass.h>

namespace EbsMagnet_ns {

// -----------------------------------------------------------------------------------------
// Create a magnet object according to type
Magnet *Magnet::Create(EbsMagnet *ds, bool focusing, double crossTalk, std::string &serial, std::string &type, std::string &model) {

  if (Magnet::compare(type, "Q")) {
    return new Quadrupole(ds, focusing, crossTalk, serial, model, type);
  } else if (Magnet::compare(type, "S")) {
    return new Sextupole(ds, focusing, crossTalk, serial, model, type);
  } else if (Magnet::compare(type, "O")) {
    return new Octupole(ds, focusing, crossTalk, serial, model, type);
  } else if (Magnet::compare(type, "DQ")) {
    return new DipoleQuadrupole(ds, focusing, crossTalk, serial, model, type);
  } else if (Magnet::compare(type, "SH3")) {
    return new SH3Magnet(ds, focusing, crossTalk, serial, model, type);
  } else if (Magnet::compare(type, "SH5")) {
    return new SH5Magnet(ds, focusing, crossTalk, serial, model, type);
  } else {
    Tango::Except::throw_exception("UnknownType",
                                   "Invalid magnet type for " + ds->get_name(),
                                   "Magnet::Create");
  }

}

// -----------------------------------------------------------------------------------------
// Default constructor
Magnet::Magnet(EbsMagnet *ds, std::string& type, std::string& serial, std::string& modelName):
        ds(ds), modelName(modelName), serial(serial), type(type) {
  parseConfig();
  model = nullptr;
};

// -----------------------------------------------------------------------------------------
Magnet::~Magnet() {
  delete model;
}

// -----------------------------------------------------------------------------------------
// Parse modelName config
// Retrieve calibration data filename and scale factor
void Magnet::parseConfig() {

  Tango::Database *db = Tango::Util::instance()->get_database();
  std::vector<Tango::DbDatum> dd;
  dd.emplace_back(Tango::DbDatum(modelName));
  db->get_property("EbsMagnet",dd);
  if(dd[0].is_empty()) {
    Tango::Except::throw_exception("ConfigError",
                                   "EbsMagnet free property: Configuration property not found for magnet modelName " + modelName,
                                   "Magnet::parseConfig");
  }

  std::vector<std::string> values;
  dd[0] >> values;
  for(auto & value : values) {
      std::vector<std::string> fields;
    split(fields,value,':');
    if(fields.size()!=2)
      Tango::Except::throw_exception("configError",
                                     "EbsMagnet free property: Invalid configuration property, name:value expected in " + value,
                                     "Magnet::parseConfig");
    ConfigItem item;
    item.name = fields[0];
    item.value = fields[1];
    config.emplace_back(item);
  }

  // Add path
  dd.clear();
  dd.emplace_back(Tango::DbDatum("_path"));
  db->get_property("EbsMagnet",dd);
  if(dd[0].is_empty()) {
    Tango::Except::throw_exception("ConfigError",
                                   "EbsMagnet free property: Configuration property _path not found",
                                   "Magnet::parseConfig");
  }

  ConfigItem item;
  item.name = "_path";
  std::string pathStr;
  dd[0] >> pathStr;
  item.value = pathStr;
  config.emplace_back(item);

}

// -----------------------------------------------------------------------------------------
// Return configuration item
std::string Magnet::getConfigItem(const std::string& name) {

  bool found = false;
  size_t i = 0;
  while(!found && i<config.size()) {
    found = compare(config[i].name,name);
    if(!found) i++;
  }
  if(!found)
    Tango::Except::throw_exception("ConfigError",
                                   modelName + " configuration item '" + name + "' not found",
                                   "Magnet::parseConfig");
  return config[i].value;

}

double Magnet::getConfigItemDouble(const std::string& name) {

    std::string s = getConfigItem(name);
  try {
    return std::stod(s);
  } catch (std::invalid_argument& e) {
    Tango::Except::throw_exception("ConfigError",
                                   modelName + " configuration '" + name + "' number expected but got " + s,
                                   "Magnet::parseConfig");
  }

}

// -----------------------------------------------------------------------------------------

bool Magnet::isStrengthH(const std::string& name) {
  return compare(name,"Strength_H");
}
bool Magnet::isStrengthV(const std::string& name) {
  return compare(name,"Strength_V");
}
bool Magnet::isStrengthSQ(const std::string& name) {
  return compare(name,"Strength_SQ");
}
bool Magnet::isStrength(const std::string& name) {
  return compare(name,"Strength");
}
bool Magnet::isResonanceStrength(const std::string& name) {
  return compare(name,"ResonanceStrength");
}
bool Magnet::isCorrectionStrength(const std::string& name) {
  return compare(name,"CorrectionStrength");
}
bool Magnet::isStrengths(const std::string& name) {
  // All strengths that require a current change
  return isStrengthH(name) || isStrengthV(name) || isStrengthSQ(name) || isStrength(name) ||
         isCorrectionStrength(name) || isResonanceStrength(name);
}
bool  Magnet::isFrozen(const std::string& name) {
  return compare(name,"Frozen") || compare(name,"Frozen_H") || compare(name,"Frozen_V") || compare(name,"Frozen_SQ");
}
bool Magnet::isMainCurrent(const std::string& name) {
  return compare(name,"MainCurrent");
}
bool Magnet::isCurrent(const std::string& name) {
  return compare(name,"MainCurrent") || compare(name,"Current");
}

// -----------------------------------------------------------------------------------------
// Add an attribute to the list
AttributeItem *Magnet::addAttribute(const std::string& name,long data_type,Tango::AttrWriteType rwType,const std::string& unit,
                                    const std::string& format,bool memorized,bool memorized_init) {

  AttributeItem item;

  auto *att = new DynAttribute(name.c_str(), data_type, rwType);
  Tango::UserDefaultAttrProp	props;
  props.set_unit(unit.c_str());
  props.set_format(format.c_str());
  att->set_default_properties(props);
  if(isStrengthH(name) || isStrengthV(name) || isStrengthSQ(name) || isMainCurrent(name))
    att->set_disp_level(Tango::EXPERT);
  if(memorized) {
    att->set_memorized();
    att->set_memorized_init(memorized_init);
  }
  item.handle = att;
  item.name = name;
  switch (data_type) {
    case Tango::DEV_BOOLEAN:
      item.read.bValue = false;
      item.set.bValue = false;
      break;
    case Tango::DEV_DOUBLE:
      item.read.dValue = NAN;
      item.set.dValue = NAN;
      break;
    default:
      Tango::Except::throw_exception("ConfigError",
                                     name + " unsupported data type",
                                     "Magnet::addAttribute");
  }
  scalarAttributes.emplace_back(item);
  return &scalarAttributes.back();

}

// -----------------------------------------------------------------------------------------
// Create Magnet attribute
void Magnet::createAttributes() {

  currentAttr = nullptr;
  strengthAtt = nullptr;
  designStrengthsAtt = nullptr;
  correctionStrengthAtt = nullptr;
  resonanceStrengthAtt = nullptr;
  frozenAtt = nullptr;
  scalarAttributes.clear();
  scalarAttributes.reserve(16);

  // Attribute from model (Strength, Strength_H, Strength_V, Strength_SQ)
  // ! Model strength attributes must at the top of the list in the model ordwer !
  std::vector<std::string> attNames;
  std::vector<std::string> attUnits;
  model->get_strength_names(attNames);
  model->get_strength_units(attUnits);
  nbStrengths = attNames.size();
  nbCurrents = (model->has_main_current()?1:0) + model->get_corrector_number();
  for(size_t i=0;i<nbStrengths;i++)
    addAttribute(attNames[i],Tango::DEV_DOUBLE, Tango::READ_WRITE,attUnits[i],"%9.6f",false,false);

  // Frozen attributes
  for(size_t i=0;i<nbStrengths;i++) {
    if(i==0 && hasMainCurrent()) {
      frozenAtt = addAttribute("Frozen", Tango::DEV_BOOLEAN, Tango::READ_WRITE, "", "%d", true,true);
    } else {
      addAttribute("Frozen" + getStrengthPrefix(attNames[i]), Tango::DEV_BOOLEAN, Tango::READ_WRITE, "", "%d", true,true);
      scalarAttributes.back().handle->set_disp_level(Tango::EXPERT);
    }
  }

  if(model->has_main_current()) {

    // DesignStrength, CorrectionStrength & ResonanceStrength for main strength
    // Strength = DesignStrength + CorrectionStrength + ResonanceStrength
    strengthAtt = &scalarAttributes[0];
    designStrengthsAtt = addAttribute("DesignStrength",Tango::DEV_DOUBLE, Tango::READ,attUnits[0],"%9.6f",false,false);
    correctionStrengthAtt = addAttribute("CorrectionStrength",Tango::DEV_DOUBLE, Tango::READ_WRITE,attUnits[0],"%9.6f",false,false);
    resonanceStrengthAtt = addAttribute("ResonanceStrength",Tango::DEV_DOUBLE, Tango::READ_WRITE,attUnits[0],"%9.6f",true,false);

    if(model->get_corrector_number()==0) {
      // Display the current when no corrector (it is a true current)
      currentAttr = addAttribute("Current",Tango::DEV_DOUBLE, Tango::READ_WRITE,"A","%6.3f",false,false);
    } else {
      // Display the current as MainCurrent and expert
      currentAttr = addAttribute("MainCurrent",Tango::DEV_DOUBLE, Tango::READ_WRITE,"A","%6.3f",false,false);
    }

  }

}

// -----------------------------------------------------------------------------------------
// Return attributes of this magnet
// Use only at initialisation time, Tango may delete them
std::vector<DynAttribute *> Magnet::getAttributes() {

    std::vector<DynAttribute *> ret;
  for(auto& att:scalarAttributes)
    ret.emplace_back(att.handle);
  return ret;

}

// -----------------------------------------------------------------------------------------
// Update strengths from currents
void Magnet::setCurrentsAndUpdateStrength(std::vector<double>& currents, std::vector<double>& setCurrents) {

  // Current attribute
  if(model->has_main_current()) {
    currentAttr->read.dValue = currents[0];
    currentAttr->set.dValue = setCurrents[0];
  }

  // Compute strengths
  std::vector<double> strengths;
  std::vector<double> setStrengths;
  try {

    model->compute_strengths(MAGNET_RIGIDITY_INV,currents,strengths);
    model->compute_strengths(MAGNET_RIGIDITY_INV,setCurrents,setStrengths);
    for(size_t i=0;i<nbStrengths;i++) {
      scalarAttributes[i].read.dValue = strengths[i];
      scalarAttributes[i].set.dValue = setStrengths[i];
    }

  } catch (std::invalid_argument& e) {

    for(size_t i=0;i<nbStrengths;i++) {
      scalarAttributes[i].read.dValue = NAN;
      scalarAttributes[i].set.dValue = NAN;
    }

  }

  updateCorrectionStrength();

}

// -----------------------------------------------------------------------------------------
// Pseudo current/current conversion (SH only)
void Magnet::computePseudoCurrentsFromCurrents(std::vector<double>& currents, std::vector<double>& pseudoCurrents) {

  model->compute_pseudo_currents_from_currents(currents,pseudoCurrents);

}

void Magnet::computeStrengthFromPseudoCurrent(int idx,double& pseudoCurrent, double& strength) {

  model->compute_strength_from_pseudo(MAGNET_RIGIDITY_INV,idx,pseudoCurrent,strength);

}

// -----------------------------------------------------------------------------------------
// Update Design strength
bool Magnet::setDesignStrength(double strength) {
  // Changing the design strength keep global strength constant
  // Difference goes into correction strength
  if(hasMainCurrent()) {
    designStrengthsAtt->set.dValue = strength;
    designStrengthsAtt->read.dValue = strength;
    updateCorrectionStrength();
    return true;
  }
  return false;
}

// -----------------------------------------------------------------------------------------
// Reset resonance strength to 0
void Magnet::resetResonanceStrength() {
  if(hasMainCurrent()) {
    resonanceStrengthAtt->read.dValue = 0.0;
    resonanceStrengthAtt->set.dValue = 0.0;
  }
}

// -----------------------------------------------------------------------------------------
// Update correction strength
void Magnet::updateCorrectionStrength() {

  if(model->has_main_current()) {

    AttributeItem *sAtt = strengthAtt;
    AttributeItem *dsAtt = designStrengthsAtt;
    AttributeItem *crAtt = correctionStrengthAtt;
    AttributeItem *rsAtt = resonanceStrengthAtt;
    crAtt->read.dValue = sAtt->read.dValue - dsAtt->read.dValue - rsAtt->read.dValue;
    crAtt->set.dValue = sAtt->set.dValue - dsAtt->set.dValue - rsAtt->set.dValue;

  }

}

// -----------------------------------------------------------------------------------------
// Return pointer to the attribute having the given name
AttributeItem *Magnet::getAttribute(const std::string& name) {

  bool found = false;
  size_t i = 0;
  while(!found && i<scalarAttributes.size()) {
    found = compare( name,scalarAttributes[i].name );
    if(!found) i++;
  }
  if(!found)
    Tango::Except::throw_exception("ConfigError",
                                   name + " attribute not found",
                                   "Magnet::getAttribute");

  return &scalarAttributes[i];

}

// -----------------------------------------------------------------------------------------
// Read attribute
void Magnet::readAttribute(Tango::Attribute &attr,std::string& name,void *readValue) {

  AttributeItem *item = getAttribute(name);
  switch( attr.get_data_type() ) {
    case Tango::DEV_BOOLEAN: {
      attr.set_value(&item->read.bValue);
      if(attr.get_writable() == Tango::READ_WRITE)
        (static_cast<Tango::WAttribute&>(attr)).set_write_value(item->set.bValue);
      if(readValue) *((Tango::DevBoolean *)readValue) = item->read.bValue;
    } break;
    default: /* double */
      attr.set_value(&item->read.dValue);
      if(attr.get_writable() == Tango::READ_WRITE)
        (static_cast<Tango::WAttribute&>(attr)).set_write_value(item->set.dValue);
      if(readValue) *((Tango::DevDouble *)readValue) = item->read.dValue;
  }

}

// -----------------------------------------------------------------------------------------
std::vector<std::string> Magnet::getSetpointCheckCommands() {

    std::vector<std::string> cmds;
  std::vector<std::string> strengthNames;
  model->get_strength_names(strengthNames);
  for(auto & strengthName : strengthNames)
    cmds.emplace_back("SetpointCheck" + getStrengthPrefix(strengthName));
  return cmds;

}

// -----------------------------------------------------------------------------------------
// Compute currents and check if currents are in the good range
bool Magnet::setpointCheck(double strength,std::string& name) {

  if(std::isnan(strength))
    return true;

  std::vector<double> strengths;
  for(size_t i=0;i<nbStrengths;i++)
    strengths.emplace_back(scalarAttributes[i].set.dValue);

  // SH 3 channels does not have a strength attribute
  int o = (nbStrengths==3)?0:1;

  if(compare(name,"SetpointCheck")) {
    if(isMainFrozen()) return true;
    strengths[0] = strength;
  } else if(compare(name,"SetpointCheck_H"))
    strengths[o] = strength;
  else if(compare(name,"SetpointCheck_V"))
    strengths[o+1] = strength;
  else if(compare(name,"SetpointCheck_SQ")) {
    strengths[o+2] = strength;
  } else
    Tango::Except::throw_exception("CommandError",
                                   name + " unexpected command",
                                   "Magnet::setpointCheck");

  return setpointCheck(strengths);

}

bool Magnet::setpointCheck(std::vector<double>& strengths) {

  if( strengths.size() != nbStrengths )
    Tango::Except::throw_exception("CommandError",
                                   "Unexpected strength number: " + std::to_string(nbStrengths) + " values expected",
                                   "Magnet::setpointCheck");

  std::vector<double> currents;
  model->compute_currents(MAGNET_RIGIDITY,strengths,currents);
  return check_minmax(currents);

}

// -----------------------------------------------------------------------------------------
// Check is given currents are OK for underlying PS
bool Magnet::check_minmax(std::vector<double>& currents,bool throwException) {

  if(!ds->minmaxAssigned)
    Tango::Except::throw_exception(
            "ErrorCommand",
            "min/max current(s) of underlying power supply(ies) not updated",
            "Magnet::check_minmax");

  bool ok = true;
  size_t i = 0;
  while(ok && i<currents.size()) {
    ok = (ds->setCurrentsMin[i] <= currents[i]) && (ds->setCurrentsMax[i] >= currents[i]);
    if(ok) i++;
  }

  if(!ok && throwException) {
    Tango::Except::throw_exception(
            "SetpointCheckError",
            ds->currentDevices[i] + ":" + std::to_string(currents[i]) + " not in " + "[" + std::to_string(ds->setCurrentsMin[i]) +
            ".." + std::to_string(ds->setCurrentsMax[i]) + "]" ,
            "Magnet::check_minmax");
  }

  return ok;

}

// -----------------------------------------------------------------------------------------
// Write attribute, return true if current(s) have to be sent to PS
void Magnet::writeAttribute(bool setValue,const std::string& name,std::vector<std::string>& psNames,std::vector<Tango::DevState>& psStates) {

  // Frozen attributes
  AttributeItem *item = getAttribute(name);
  item->read.bValue = setValue;
  item->set.bValue = setValue;

}

void Magnet::writeAttribute(double setValue,const std::string& name,std::vector<std::string>& psNames,std::vector<Tango::DevState>& psStates) {

    std::vector<double> newCurrents;

  // Check if allowed
  bool isStrength = isStrengths(name);
  if( isStrength ) {
    // Frozen check
      std::string frozenName = "Frozen" + getStrengthPrefix(name);
    frozenCheck(frozenName);
    // Check underlying PS states
    psCheck(name,psNames,psStates);
  }

  if(isCurrent(name)) {
    frozenCheck("Frozen");
    AttributeItem *item = getAttribute(name);
    newCurrents.resize(1);
    newCurrents[0] = setValue;
    check_minmax(newCurrents, true);
    item->set.dValue = newCurrents[0];
    ds->setCurrents[0] = newCurrents[0];
    send_currents();
    return;
  }

  // Make a backup
  std::vector<double> oldSetpoints;
  oldSetpoints.resize(scalarAttributes.size());
  for(size_t i=0;i<scalarAttributes.size();i++)
    oldSetpoints[i] = scalarAttributes[i].set.dValue;

  AttributeItem *item = getAttribute(name);
  item->set.dValue = setValue;

  if(isStrength) {

    if(isCorrectionStrength(name)) {
      AttributeItem *rsAtt = resonanceStrengthAtt;
      AttributeItem *dsAtt = designStrengthsAtt;
      // Update new main strength
      scalarAttributes[0].set.dValue = item->set.dValue + rsAtt->set.dValue + dsAtt->set.dValue;
    }

    if(isResonanceStrength(name)) {
      AttributeItem *crAtt = correctionStrengthAtt;
      AttributeItem *dsAtt = designStrengthsAtt;
      // Update new main strength
      scalarAttributes[0].set.dValue = item->set.dValue + crAtt->set.dValue + dsAtt->set.dValue;
      item->read.dValue = item->set.dValue;
    }

    std::vector<double> strengths;
    for(size_t i=0;i<nbStrengths;i++)
      strengths.emplace_back(scalarAttributes[i].set.dValue);

    try {
      model->compute_currents(MAGNET_RIGIDITY, strengths, newCurrents);
    } catch (std::invalid_argument& e) {
      // Should not happen
      Tango::Except::throw_exception("ModelError",
                                     e.what(),
                                     "Magnet::writeAttribute");
    }

    // Check setpoints, throw exception in case of setpoint out of bounds
    try {
      check_minmax(newCurrents, true);
    } catch (Tango::DevFailed& e) {
      // Restore backup
      for(size_t i=0;i<scalarAttributes.size();i++)
        scalarAttributes[i].set.dValue = oldSetpoints[i];
      throw e;
    }

    for(size_t i=0;i<nbCurrents;i++)
      ds->setCurrents[i] = newCurrents[i];
    // Update pseudo currents (SH only)
    if(ds->isSH)
      computePseudoCurrentsFromCurrents(newCurrents,ds->setPseudoCurrents);
    send_currents();

  }

}

void Magnet::writeStrengths(std::vector<double>& strengths) {

  setpointCheck(strengths);
  std::vector<double> newCurrents;
  model->compute_currents(MAGNET_RIGIDITY, strengths, newCurrents);
  for(size_t i=0;i<nbCurrents;i++)
    ds->setCurrents[i] = newCurrents[i];
  send_currents();

}

void Magnet::readStrengths(double *strengths) {

  for(size_t i=0;i<nbStrengths;i++)
    strengths[i] = scalarAttributes[i].set.dValue;

}

// -----------------------------------------------------------------------------------------
// Send surrents to power supplies

void Magnet::send_currents() {

  ds->attr_RequireCycling_read[0] = true;

  std::vector<Tango::DeviceAttribute> attributes;
  for(double setCurrent : ds->setCurrents) {
    Tango::DeviceAttribute attribute("Current", setCurrent);
    attributes.push_back(attribute);
  }

  Tango::GroupReplyList arl = ds->psGroup->write_attribute(attributes);

  if (arl.has_failed()) {
      std::string error;
    for (size_t i = 0; i < arl.size(); i++)
      if (arl[i].has_failed())
        error += ds->currentDevices[i] + ":" +
                 std::string(arl[i].get_err_stack()[0].desc) + "\n";
    Tango::Except::throw_exception(
            "ErrorWrite",
            error.c_str(),
            "Magnet::send_currents"
    );
  }

}

// -----------------------------------------------------------------------------------------
// Get correction strength setpoint
double Magnet::getCorrectionStrength() {

  return correctionStrengthAtt->set.dValue;

}

// -----------------------------------------------------------------------------------------
// Get resonance strength setpoint
double Magnet::getResonanceStrength() {
  return resonanceStrengthAtt->set.dValue;
}

void Magnet::setResonanceStrength(double set) {
  resonanceStrengthAtt->read.dValue = set;
  resonanceStrengthAtt->set.dValue = set;
}

// -----------------------------------------------------------------------------------------
// Return if main strength is frozen
bool Magnet::isMainFrozen() {
  if(hasMainCurrent()) {
    return frozenAtt->set.bValue;
  } else {
    return false;
  }
}

// -----------------------------------------------------------------------------------------
// Throw an exception if the corresponding strength is frozen
void Magnet::frozenCheck(const std::string& name) {
  AttributeItem *frozen = getAttribute(name);
  if(frozen->set.bValue)
    Tango::Except::throw_exception("Frozen",
                                   "Device is frozen",
                                   "Magnet::writeAttribute");
}

// -----------------------------------------------------------------------------------------
// Throw an exception if an underlying power supply is not ON
void Magnet::psCheck(const std::string& name,std::vector<std::string>& names,std::vector<Tango::DevState>& states) {

  size_t i = 0;
  bool ok = true;
  std::string mesg;
  while(i<states.size()) {
    if( states[i]!=Tango::ON && states[i]!=Tango::ALARM && states[i]!=Tango::STANDBY) {
      mesg += names[i]+":"+Tango::DevStateName[states[i]]+"\n";
      ok = false;
    }
    i++;
  }

  if(!ok) {
    mesg = "Cannot write " + name + ": One or more underlying PS not ON\n" + mesg;
    mesg.pop_back();
    Tango::Except::throw_exception("ErrorWrite",
                                    mesg,
                                   "Magnet::psCheck");
  }

}

// -----------------------------------------------------------------------------------------
// Util functions
// -----------------------------------------------------------------------------------------
std::string Magnet::getStrengthPrefix(const std::string& name) {

    std::string nameLower;
  nameLower.resize(name.size());
  std::transform(name.begin(), name.end(), nameLower.begin(),
                 [](unsigned char c){ return std::tolower(c); });

  std::size_t pos = nameLower.find("strength");
  if(pos==std::string::npos)
    Tango::Except::throw_exception("CallError",
                                   name + " not a strength attribute",
                                   "Magnet::getAttribute");

  std::string pref = name.substr(pos+8);

  return pref;

}

// compare string (ignore case)
bool Magnet::compare(const std::string& s1,const std::string& s2) {

  return ::strcasecmp(s1.c_str(),s2.c_str())==0;

}

// Trim the given string
std::string trim(const std::string &s)
{
  auto start = s.begin();
  while (start != s.end() && std::isspace(*start))
    start++;
  auto end = s.end();
  do {
    end--;
  } while (std::distance(start, end) > 0 && std::isspace(*end));

  return std::string(start, end + 1);
}

// Split given string into tokens using specified separator
void Magnet::split(std::vector<std::string> &tokens, const std::string &text, char sep) {

  size_t start = 0, end = 0;
  tokens.clear();
  while ((end = text.find(sep, start)) != std::string::npos) {
    tokens.push_back(trim(text.substr(start, end - start)));
    start = end + 1;
  }
  tokens.push_back(trim(text.substr(start)));

}

}

#include <Magnet/Magnet.h>
#include <Quadrupole.h>
#include <Sextupole.h>
#include <Octupole.h>
#include <DipoleQuadrupole.h>
#include <SH3Magnet.h>
#include <SH5Magnet.h>
#include <tango/classes/EbsMagnet/EbsMagnet.h>

#ifndef EBSMAGNET_QUADRUPOLE_H
#define EBSMAGNET_QUADRUPOLE_H

namespace EbsMagnet_ns {

class EbsMagnet;

class Quadrupole : public Magnet {
 public:
    Quadrupole(EbsMagnet *ds,bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type);
};

class Sextupole : public Magnet {
public:
  Sextupole(EbsMagnet *ds,bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type);
};

class Octupole : public Magnet {
public:
  Octupole(EbsMagnet *ds,bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type);
};

class DipoleQuadrupole : public Magnet {
public:
  DipoleQuadrupole(EbsMagnet *ds,bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type);
};

class SH3Magnet : public Magnet {
public:
  SH3Magnet(EbsMagnet *ds,bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type);
};

class SH5Magnet : public Magnet {
public:
  SH5Magnet(EbsMagnet *ds,bool focusing,double crossTalk,std::string &serial,std::string &magModel,std::string& type);
};

} // end namespace EbsMagnet_ns



#endif //EBSMAGNET_QUADRUPOLE_H

# Project EbsMagnet

Tango c++ class to handle EBS magnets (and correctors). The EbsMagnet class ensures current to strength conversions and reciprocal conversions. All EbsMagnet objects export all magnetic component of a magnet, if relevant. (main strength,steering strengths and skew quad strength).

![Magnet software overview](https://gitlab.esrf.fr/accelerators/Magnets/EbsMagnet/-/raw/main/doc/architecture.jpg)

## Global configuration (Calibration data)

![Magnet Global configuration](https://gitlab.esrf.fr/accelerators/Magnets/EbsMagnet/-/raw/main/doc/config1.jpg)

## Magnet configuration

![Magnet configuration](https://gitlab.esrf.fr/accelerators/Magnets/EbsMagnet/-/raw/main/doc/config2.jpg)

## Cloning

```
git clone git@gitlab.esrf.fr:accelerators/Magnets/EbsMagnet.git
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* Tango Controls 9 or higher
* omniORB release 4 or higher
* libzmq
* MagnetModel library

To compile, your project structure must looks like 

#### Toolchain Dependencies 

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build. 

### Build

Instructions on building the project.

CMake example: 

```bash
cd project_name
mkdir build
cd build
cmake ../
make
```

#
